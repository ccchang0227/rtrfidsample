//
//  AppDelegate.h
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

