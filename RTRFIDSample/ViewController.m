//
//  ViewController.m
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "ViewController.h"
#import <RT_RFID_SDK/RT_RFID_SDK.h>

@interface ViewController () <RTAcsBtCommunicatorDelegate, RTRfidCommunicatorDelegate, RTFeitianAjCommunicatorDelegate>

@property (weak, nonatomic) IBOutlet UITextView *msgTextView;

@property (strong, nonatomic) RTAcsBtCommunicator *c1;
@property (strong, nonatomic) RTAcsBtCommunicator *c2;

@property (weak, nonatomic) RTFeitianAjCommunicator *c3;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.msgTextView.text = @"Starting...\n-------------------------------------\n";
    self.msgTextView.contentOffset = CGPointZero;
    
//    self.c1 = [RTAcsBtCommunicator communicatorWithSpecificDeviceID:@"RR330-005230"];
//    self.c1.delegate = self;
//    [self.c1 start];
//
    self.c2 = [RTAcsBtCommunicator communicatorWithSpecificDeviceID:@"004180"];
    self.c2.delegate = self;
    [self.c2 start];
    
//    self.c3 = [RTFeitianAjCommunicator sharedInstance];
//    self.c3.delegate = self;
//    self.c3.scanPeriod = RTFeitianAjScanPeriodFast;
//    [self.c3 start];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self.c1 stop];
    
//    [self.c2 stop];
    
    [self.c3 stop];
    
}

#pragma mark -

- (NSString *)identifierTitleWithCommunicator:(RTRfidCommunicator *)communicator {
    if (communicator == self.c1) {
        return @"Reader 1";
    }
    else if (communicator == self.c2) {
        return @"Reader 2";
    }
    
    return @"???";
}

- (void)scrollToLastLine {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CGFloat y = self.msgTextView.contentSize.height-self.msgTextView.frame.size.height;
        y = MAX(y, 0);
        
        [self.msgTextView setContentOffset:CGPointMake(0, y) animated:YES];
    });
    
}

- (NSString *)ABD_stringFromCardStatus:(ABTBluetoothReaderCardStatus)cardStatus {
    
    NSString *string;
    
    switch (cardStatus) {
            
        case ABTBluetoothReaderCardStatusUnknown:
            string = @"Unknown";
            break;
            
        case ABTBluetoothReaderCardStatusAbsent:
            string = @"Absent";
            break;
            
        case ABTBluetoothReaderCardStatusPresent:
            string = @"Present";
            break;
            
        case ABTBluetoothReaderCardStatusPowered:
            string = @"Powered";
            break;
            
        case ABTBluetoothReaderCardStatusPowerSavingMode:
            string = @"Power Saving Mode";
            break;
            
        default:
            string = @"Unknown";
            break;
    }
    
    return string;
}

- (IBAction)clearConsole:(id)sender {
    self.msgTextView.text = @"";
    self.msgTextView.contentOffset = CGPointZero;
}

#pragma mark - RTAcsBtCommunicatorDelegate

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator didConnectToReader:(ABTBluetoothReader *)reader {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"reader: %@", reader);
    
    NSString *readerId = [self identifierTitleWithCommunicator:communicator];
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"%@:didConnectToReader: %@\n", readerId, communicator.readerName];
    [text appendFormat:@"%@:  serial number: %@\n", readerId, communicator.serialNumber];
    [text appendFormat:@"%@:  model number: %@\n", readerId, communicator.modelNumber];
    [text appendFormat:@"%@:  system ID: %@\n", readerId, communicator.systemID];
    [text appendFormat:@"%@:  firmware version: %@\n", readerId, communicator.firmwareVersion];
    [text appendFormat:@"%@:  hardware version: %@\n", readerId, communicator.hardwareVersion];
    [text appendFormat:@"%@:  manufacturer: %@\n", readerId, communicator.manufacturerName];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
    
}

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
        didChangeCardStatus:(ABTBluetoothReaderCardStatus)cardStatus {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"cardStatus: %@", [self ABD_stringFromCardStatus:cardStatus]);
    
    NSString *readerId = [self identifierTitleWithCommunicator:communicator];
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"%@:didChangeCardStatus: %@\n", readerId, [self ABD_stringFromCardStatus:cardStatus]];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
    
}

#pragma mark - RTFeitianAjCommunicatorDelegate

- (void)rtFeitianAjCommunicatorDidConnectToReader:(RTFeitianAjCommunicator *)communicator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"didConnectToReader: %@\n", communicator.deviceID];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
}

- (void)rtFeitianAjCommunicator:(RTFeitianAjCommunicator *)communicator
                     didFindTag:(NSString *)tagID
                        withAtr:(NSString *)atrString
                          error:(NSError *)error {
    
}

#pragma mark - RTRfidCommunicatorDelegate

- (void)rtRfidCommunicator:(RTRfidCommunicator *)communicator didReceiveError:(NSError *)error {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"error: %@", error);
    
    NSString *readerId = [self identifierTitleWithCommunicator:communicator];
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"%@:didReceiveError: %@\n", readerId, error];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
    
}

- (void)rtRfidCommunicatorDidDisconnectReader:(RTRfidCommunicator *)communicator withError:(NSError *)error {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"error: %@", error);
    
    NSString *readerId = [self identifierTitleWithCommunicator:communicator];
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"%@:didDisconnectReader: %@\n", readerId, error];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [communicator start];
    });
    
}

- (void)rtRfidCommunicator:(RTRfidCommunicator *)communicator didFindTag:(NSString *)tagID withAtr:(NSString *)atrString error:(NSError *)error {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"tagID: %@", tagID);
    NSLog(@"ATR: %@", atrString);
    NSLog(@"error: %@", error);
    
    NSString *readerId = [self identifierTitleWithCommunicator:communicator];
    
    NSMutableString *text = [NSMutableString stringWithString:self.msgTextView.text];
    [text appendFormat:@"%@:didFindTag: %@\n", readerId, tagID];
    [text appendFormat:@"%@:  ATR: %@\n", readerId, atrString];
    [text appendFormat:@"%@:  error: %@\n", readerId, error];
    [text appendFormat:@"-------------------------------------\n"];
    self.msgTextView.text = text;
    
    [self scrollToLastLine];
    
    [communicator powerOffCard];
    
}

@end
