//
//  RTFeitianAjCommunicator.m
//  RT_RFID_SDK
//
//  Created by realtouchapp on 2017/10/30.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "RTFeitianAjCommunicator.h"
#include "utils.h"


typedef NS_ENUM(NSInteger, RTFeitianAjCardType) {
    RTFeitianAjCardTypeMifare1K,
    RTFeitianAjCardTypeA,
    RTFeitianAjCardTypeB,
    RTFeitianAjCardTypeFelica,
    RTFeitianAjCardTypeTopaz
};

#define RTFeitianAjErrorDomain @"RTFeitianAjErrorDomain"

@interface RTFeitianAjCommunicator () {
    BOOL _readerConnected;
    BOOL _isListening;
    
    dispatch_source_t _scanCardTimer;
    
    nfc_card_t _NFC_Card;
}

@property (strong, nonatomic) NSString *deviceID;
@property (strong, nonatomic) NSString *deviceUID;
@property (strong, nonatomic) NSString *firmwareVersion;

@end

@implementation RTFeitianAjCommunicator
@dynamic delegate;

+ (instancetype)sharedInstance {
    static RTFeitianAjCommunicator *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[RTFeitianAjCommunicator alloc] init];
    });
    
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _readerConnected = NO;
        self.deviceID = nil;
        self.deviceUID = nil;
        self.firmwareVersion = nil;
        
        _isListening = NO;
        
        _ar530Reader = [FTaR530 sharedInstance];
        [_ar530Reader setDeviceEventDelegate:self];
        _ar530Reader.cardType = 0x0f;//A_CARD | B_CARD | Felica_CARD | Topaz_CARD;
        
        // Here we need waiting until the device has initialized
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self->_ar530Reader getDeviceID:self];
        });
    }
    return self;
}

- (void)dealloc {
    if (_scanCardTimer != NULL) {
        dispatch_source_cancel(_scanCardTimer);
    }
    _scanCardTimer = NULL;
}

#pragma mark - Getter

- (FTaR530 *)ar530Reader {
    return _ar530Reader;
}

- (NSString *)ar530LibVersion {
    return [_ar530Reader getLibVersion];
}

#pragma mark - Public

- (void)start {
    _isListening = YES;
    
    if (_scanCardTimer != NULL) {
        dispatch_source_cancel(_scanCardTimer);
    }
    _scanCardTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(_scanCardTimer, DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC, 0.05 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(_scanCardTimer, ^ {
        if (!self->_isListening) {
            dispatch_source_cancel(self->_scanCardTimer);
            self->_scanCardTimer = NULL;
            return;
        }
        if (!self->_readerConnected) {
            return;
        }
        
        [self->_ar530Reader NFC_Card_Open:self];

    });
    dispatch_resume(_scanCardTimer);
    
}

- (void)stop {
    _isListening = NO;
    
    if (_scanCardTimer != NULL) {
        dispatch_source_cancel(_scanCardTimer);
    }
    _scanCardTimer = NULL;
    
}

- (void)sendApdu:(NSData *)apdu {
    if (_NFC_Card != 0) {
        [_ar530Reader NFC_Card_Transmit:_NFC_Card sendBuf:(unsigned char *)apdu.bytes sendLen:(unsigned int)apdu.length delegate:self];
    }
}

- (void)powerOffCard {
    if (_NFC_Card != 0) {
        [_ar530Reader NFC_Card_Close:_NFC_Card delegate:self];
    }
    _NFC_Card = 0;
    
}

#pragma mark - Private

- (void)_postDelegateError:(NSError *)error {
    if (!_isListening) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicator:didReceiveError:)]) {
            [self.delegate rtRfidCommunicator:self didReceiveError:error];
        }
    });
    
}

- (void)_getConnectResult:(nfc_card_t)cardHandle {
    _NFC_Card = cardHandle;
    if (cardHandle != 0) {
        [_ar530Reader NFC_Card_Recognize:_NFC_Card delegate:self];
    }
    else {
//        NSError *error = [NSError errorWithDomain:RTFeitianAjErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:@"NFC_card_open failed", NSLocalizedFailureReasonErrorKey:@"NFC_card_open failed"}];
//        [self _postDelegateError:error];
    }
    
}

- (void)_getRecognizeResult:(nfc_card_t)cardHandle cardType:(unsigned int)errCode {
    if (errCode == 0xFF) {
        NSError *error = [NSError errorWithDomain:RTFeitianAjErrorDomain code:-8002 userInfo:@{NSLocalizedDescriptionKey:@"Recongnize Card failed", NSLocalizedFailureReasonErrorKey:@"Recongnize Card failed"}];
        [self _postDelegateError:error];
        
        return;
    }
    
    unsigned int cardT = 0;
    NSString *tempkeyType = @"";
    char uid[128] = {0};
    char IDm[16 + 1] = {0};
    char PMm[16 + 1] = {0};
    char pupi[64] = {0};
//    char atqa[4+1] = {0};
    
    HexToStr(uid, cardHandle->uid, cardHandle->uidLen);
    
    cardT = errCode;
    _readerConnected = YES;
    
    NSString *cardNumber = nil;
    NSString *atrString = nil;
    NSError *error = nil;
    
    NS_DURING
    NSData *data = [NSData dataWithBytes:cardHandle->atr length:cardHandle->atrlen];
    atrString = [NSString stringWithUTF8String:data.bytes];
    NS_HANDLER
    NS_ENDHANDLER
    
    _NFC_Card = cardHandle;
    
    // get the card type
    if (cardHandle->type == CARD_TYPE_A) {
        tempkeyType = @"A";
        if (cardT == CARD_NXP_MIFARE_1K || cardT == CARD_NXP_MIFARE_4K) {
            RTRfidLog(@"Card Type:Mifare 1K, SAK:%02x UID:%s", cardHandle->SAK, uid);
            
            cardNumber = [NSString stringWithUTF8String:uid];
            
        }
        else if(cardT == CARD_NXP_DESFIRE_EV1) {
            RTRfidLog(@"Card Type:A, SAK:%02x UID:%s", cardHandle->SAK, uid);
            
            cardNumber = [NSString stringWithUTF8String:uid];
            
        }
        else if(cardT == CARD_NXP_MIFARE_UL) {
            RTRfidLog(@"Card Type:Mifare 1K, SAK:%02x UID:%s", cardHandle->SAK, uid);
            
            cardNumber = [NSString stringWithUTF8String:uid];
            
        }
        else {
            RTRfidLog(@"Card Type:A, SAK:%02x UID:%s", cardHandle->SAK, uid);
            
            cardNumber = [NSString stringWithUTF8String:uid];
            
        }
    }
    else if (cardHandle->type == CARD_TYPE_B) {
        // PUPI: 唯一識別符
        tempkeyType = @"B";
        if (cardT == CARD_NXP_M_1_B) {
            HexToStr(pupi, cardHandle->PUPI, cardHandle->PUPILen);
            RTRfidLog(@"Card Type:B, ATQB:%02x PUPI:%s", cardHandle->ATQB, pupi);
            
            cardNumber = [NSString stringWithUTF8String:pupi];
            
        }
        else if (cardT == CARD_NXP_TYPE_B) {
            HexToStr(pupi, cardHandle->PUPI, cardHandle->PUPILen);
            RTRfidLog(@"Card Type:B, ATQB:%02x PUPI:%s", cardHandle->ATQB, pupi);
            
            cardNumber = [NSString stringWithUTF8String:pupi];
            
        }
    }
    else if (cardHandle->type == CARD_TYPE_C) {                  // Felica
        // IDm: 唯一識別符
        HexToStr(IDm, cardHandle->IDm, 8);
        HexToStr(PMm, cardHandle->PMm, 8);
        RTRfidLog(@"Card Type:Felica, Felica_ID:%s, Pad_ID:%s", IDm, PMm);
        
        cardNumber = [NSString stringWithUTF8String:IDm];
        
    }
    else if (cardHandle->type == CARD_TYPE_D) {                  // Topaz
        HexToStr(pupi, cardHandle->PUPI, cardHandle->PUPILen);
        RTRfidLog(@"Card Type:Topaz, ATQA:%2s ID1z:%s", cardHandle->ATQA, pupi);
        
        cardNumber = [NSString stringWithUTF8String:pupi];
        
    }
    else {
        RTRfidLog(@"Unknown type of card!");
        
        error = [NSError errorWithDomain:RTFeitianAjErrorDomain code:-8000 userInfo:@{NSLocalizedDescriptionKey:@"Unknown type of card", NSLocalizedFailureReasonErrorKey:@"Unknown type of card"}];
        
        _NFC_Card = 0;
    }
    
    if (_isListening) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicator:didFindTag:withAtr:error:)]) {
                [self.delegate rtRfidCommunicator:self
                                       didFindTag:cardNumber
                                          withAtr:atrString
                                            error:error];
            }
        });
    }
    
}

#pragma mark - FTaR530Delegate

- (void)FTaR530DidConnected {
    RTRfidLog(@"FTaR530DidConnected");
    
    if (_readerConnected) {
        return;
    }
    
    // Here we need waiting until the device has initialized
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self->_ar530Reader getDeviceID:self];
    });
    
}

- (void)FTaR530DidDisconnected {
    RTRfidLog(@"FTaR530DidDisconnected");
    
    if (!_readerConnected) {
        return;
    }
    
    _readerConnected = NO;
    self.deviceID = nil;
    self.deviceUID = nil;
    self.firmwareVersion = nil;
    
    if (_isListening) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicatorDidDisconnectReader:withError:)]) {
                [self.delegate rtRfidCommunicatorDidDisconnectReader:self withError:nil];
            }
        });
    }
    
}

- (void)FTNFCDidComplete:(nfc_card_t)cardHandle retData:(unsigned char *)retData retDataLen:(unsigned int)retDataLen functionNum:(unsigned int)funcNum errCode:(unsigned int)errCode {
    
    switch (funcNum) {
        case FT_FUNCTION_NUM_OPEN_CARD: {
            RTRfidLog(@"FT_FUNCTION_NUM_OPEN_CARD");
            [self _getConnectResult:cardHandle];
            break;
        }
        case FT_FUNCTION_NUM_RECOGNIZE: {
            RTRfidLog(@"FT_FUNCTION_NUM_RECOGNIZE");
            [self _getRecognizeResult:cardHandle cardType:errCode];
            break;
        }
        default: {
            RTRfidLog(@"functionNum: %ud", funcNum);
            break;
        }
    }
    
}

- (void)FTaR530GetInfoDidComplete:(unsigned char *)retData retDataLen:(unsigned int)retDataLen  functionNum:(unsigned int)functionNum errCode:(unsigned int)errCode {
    
    NSString *retString = [NSString stringWithUTF8String:(char *)retData];
    switch (functionNum) {
        case FT_FUNCTION_NUM_GET_DEVICEID: {
            RTRfidLog(@"FT_FUNCTION_NUM_GET_DEVICEID");
            
            if (retString.length <= 0) {
                _readerConnected = NO;
                return;
            }
            
            // device ID
            self.deviceID = retString;
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self->_ar530Reader getFirmwareVersion:self];
            });
            
            break;
        }
        case FT_FUNCTION_NUM_GET_DEVICEUID: {
            RTRfidLog(@"FT_FUNCTION_NUM_GET_DEVICEUID");
            
            // device UID
            self.deviceUID = retString;
            
            _readerConnected = YES;
            
            if (_isListening) {
                dispatch_async(dispatch_get_main_queue(), ^ {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(rtFeitianAjCommunicatorDidConnectToReader:)]) {
                        [self.delegate rtFeitianAjCommunicatorDidConnectToReader:self];
                    }
                });
            }
            
            break;
        }
        case FT_FUNCTION_NUM_GET_FIRMWAREVERSION: {
            RTRfidLog(@"FT_FUNCTION_NUM_GET_FIRMWAREVERSION");
            
            // firmware version
            self.firmwareVersion = retString;
        
            // get device UID
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self->_ar530Reader getDeviceUID:self];
            });
            
            break;
        }
        default:
            break;
    }
    
}

@end
