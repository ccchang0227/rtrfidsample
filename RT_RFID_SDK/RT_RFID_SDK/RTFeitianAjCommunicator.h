//
//  RTFeitianAjCommunicator.h
//  RT_RFID_SDK
//
//  Created by realtouchapp on 2017/10/30.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "RTRfidCommunicator.h"
#import "FTaR530.h"

@protocol RTFeitianAjCommunicatorDelegate;

@interface RTFeitianAjCommunicator : RTRfidCommunicator <FTaR530Delegate> {
@package
    FTaR530 *_ar530Reader;
}

+ (instancetype)sharedInstance;
- (instancetype)init;

@property (weak, nonatomic) id<RTFeitianAjCommunicatorDelegate, RTRfidCommunicatorDelegate> delegate;

@property (readonly, nonatomic) FTaR530 *ar530Reader;

@property (readonly, nonatomic) NSString *ar530LibVersion;
@property (readonly, nonatomic) NSString *deviceID;
@property (readonly, nonatomic) NSString *deviceUID;
@property (readonly, nonatomic) NSString *firmwareVersion;

@end

@protocol RTFeitianAjCommunicatorDelegate <RTRfidCommunicatorDelegate>
@optional

- (void)rtFeitianAjCommunicatorDidConnectToReader:(RTFeitianAjCommunicator *)communicator;

@end
