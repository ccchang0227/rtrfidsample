//
//  RTAcsBtCommunicator.m
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "RTAcsBtCommunicator.h"
#import "ABDHex.h"


#define RTAcsBtErrorDomain @"RTAcsBtErrorDomain"
NSString *const RTAcsBtCommunicatorTransmitPowerOptionKey = @"RTAcsBtCommunicatorTransmitPowerOptionKey";
NSString *const RTAcsBtCommunicatorSleepModeOptionKey = @"RTAcsBtCommunicatorSleepModeOptionKey"
;

typedef NS_ENUM(NSInteger, RTAcsBtEscCmdType) {
    RTAcsBtEscCmdTypeNone,
    RTAcsBtEscCmdTypeTransmitPower,
    RTAcsBtEscCmdTypeSleepMode,
    RTAcsBtEscCmdTypeEnablePolling,
    RTAcsBtEscCmdTypeDisablePolling,
    RTAcsBtEscCmdTypeGetTagID,
    RTAcsBtEscCmdTypeOthers
};

@interface RTAcsBtReaderInfo : NSObject

@property (strong, nonatomic) NSString *systemID;
@property (strong, nonatomic) NSString *modelNumber;
@property (strong, nonatomic) NSString *serialNumber;
@property (strong, nonatomic) NSString *firmwareVersion;
@property (strong, nonatomic) NSString *hardwareVersion;
@property (strong, nonatomic) NSString *manufacturerName;

@end

@implementation RTAcsBtReaderInfo
@end


@interface RTAcsBtCommunicator () {
    
    BOOL _isStarted;
    BOOL _isListening;
}

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *peripheral;

@property (strong, nonatomic) NSMutableArray *peripherals;

@property (strong, nonatomic) ABTBluetoothReaderManager *bluetoothReaderManager;
@property (strong, nonatomic) ABTBluetoothReader *bluetoothReader;

@property (strong, nonatomic) RTAcsBtReaderInfo *connectedReaderInfo;

@property (nonatomic) RTAcsBtEscCmdType escapeCmdType;
@property (strong, nonatomic) NSData *escapeCommand;

@property (strong, nonatomic) NSString *atrString;

@property (strong, nonatomic) NSError *customError;

@end

@implementation RTAcsBtCommunicator
@dynamic delegate;
@synthesize centralManager = _centralManager;
@synthesize peripheral = _peripheral;
@synthesize bluetoothReaderManager = _bluetoothReaderManager;
@synthesize bluetoothReader = _bluetoothReader;

+ (instancetype)communicator {
    return [[[self class] alloc] init];
}

+ (instancetype)communicatorWithSpecificDeviceID:(NSString *)specificDeviceID {
    return [[[self class] alloc] initWithSpecificDeviceID:specificDeviceID];
}

+ (instancetype)communicatorWithSpecificDeviceID:(NSString *)specificDeviceID options:(NSDictionary *)options {
    return [[[self class] alloc] initWithSpecificDeviceID:specificDeviceID options:options];
}

- (instancetype)init {
    return [self initWithSpecificDeviceID:nil];
}

- (instancetype)initWithSpecificDeviceID:(NSString *)specificDeviceID {
    NSDictionary *options = @{RTAcsBtCommunicatorTransmitPowerOptionKey : @(RTAcsBtTransmitPowerVeryLow),
                              RTAcsBtCommunicatorSleepModeOptionKey : @(RTAcsBtSleepModeNone)};
    return [self initWithSpecificDeviceID:specificDeviceID options:options];
}

- (instancetype)initWithSpecificDeviceID:(NSString *)specificDeviceID options:(NSDictionary *)options {
    self = [super init];
    if (self) {
        _specificDeviceId = specificDeviceID;
        
        if ([options.allKeys containsObject:RTAcsBtCommunicatorTransmitPowerOptionKey]) {
            _transmitPower = [options[RTAcsBtCommunicatorTransmitPowerOptionKey] integerValue];
        }
        else {
            _transmitPower = RTAcsBtTransmitPowerVeryLow;
        }
        if ([options.allKeys containsObject:RTAcsBtCommunicatorSleepModeOptionKey]) {
            _sleepMode = [options[RTAcsBtCommunicatorSleepModeOptionKey] integerValue];
        }
        else {
            _sleepMode = RTAcsBtSleepModeNone;
        }
        
        _escapeCmdType = RTAcsBtEscCmdTypeNone;
        
        NSDictionary *option = @{ CBCentralManagerOptionShowPowerAlertKey : @YES };
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:option];
        _peripherals = [[NSMutableArray alloc] init];
        _bluetoothReaderManager = [[ABTBluetoothReaderManager alloc] init];
        _bluetoothReaderManager.delegate = self;
        
        _isStarted = NO;
        _isListening = NO;
    }
    return self;
}

- (void)dealloc {
    [_centralManager stopScan];
    [_peripherals removeAllObjects];
    if (_bluetoothReader) {
        [_bluetoothReader detach];
    }
    if (_peripheral) {
        [_centralManager cancelPeripheralConnection:_peripheral];
    }
}

#pragma mark - Setter

- (void)setSpecificDeviceId:(NSString *)specificDeviceId {
    if (_specificDeviceId != specificDeviceId) {
        _specificDeviceId = specificDeviceId;
        
        [self.centralManager stopScan];
        
        if (self.bluetoothReader) {
            [self.bluetoothReader detach];
            self.bluetoothReader = nil;
            
            self.customError = nil;
            
            if (self.peripheral) {
                [self.centralManager cancelPeripheralConnection:self.peripheral];
            }
            self.peripheral = nil;
            
            self.connectedReaderInfo = nil;
            self.escapeCommand = nil;
            self.atrString = nil;
            
            if (_isStarted && _isListening) {
                [self _startBtScan];
            }
            
        }
    }
}

#pragma mark - Getter

- (NSString *)readerName {
    return self.peripheral.name;
}

- (NSString *)serialNumber {
    return self.connectedReaderInfo.serialNumber;
}

- (NSString *)firmwareVersion {
    return self.connectedReaderInfo.firmwareVersion;
}

- (NSString *)hardwareVersion {
    return self.connectedReaderInfo.hardwareVersion;
}

- (NSString *)systemID {
    return self.connectedReaderInfo.systemID;
}

- (NSString *)modelNumber {
    return self.connectedReaderInfo.modelNumber;
}

- (NSString *)manufacturerName {
    return self.connectedReaderInfo.manufacturerName;
}

#pragma mark - Public

- (void)start {
    _isStarted = YES;
    _isListening = YES;
    
    [self.centralManager stopScan];
    
    if (!self.peripheral || !self.bluetoothReader) {
        
        if (self.centralManager.state == CBCentralManagerStatePoweredOn) {
            [self _startBtScan];
        }
        
    }
    
}

- (void)stop {
    _isListening = NO;
    
    [self.centralManager stopScan];
    
}

- (void)forceDisconnect {
    [self stop];
    
    if (self.peripheral) {
        if (self.bluetoothReader) {
            [self.bluetoothReader detach];
            self.bluetoothReader = nil;
        }
        
        self.customError = nil;
        
        [self.centralManager cancelPeripheralConnection:self.peripheral];
        self.peripheral = nil;
    }
    
    self.connectedReaderInfo = nil;
    self.escapeCmdType = RTAcsBtEscCmdTypeNone;
    self.escapeCommand = nil;
    self.atrString = nil;
    
}

- (void)sendEscapeCommand:(NSData *)escapeCommand {
    if (self.bluetoothReader) {
        [self.bluetoothReader transmitEscapeCommand:escapeCommand];
    }
}

- (void)sendApdu:(NSData *)apdu {
    if (self.bluetoothReader) {
        [self.bluetoothReader transmitApdu:apdu];
    }
}

- (void)powerOffCard {
    if (self.bluetoothReader) {
        [self.bluetoothReader powerOffCard];
    }
}

#pragma mark - Private

- (void)_postDelegateError:(NSError *)error {
    if (!_isListening) {
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicator:didReceiveError:)]) {
        [self.delegate rtRfidCommunicator:self didReceiveError:error];
    }
}

- (void)_startBtScan {
    [self.peripherals removeAllObjects];
    
    // Scan the peripherals.
    NSDictionary *option = @{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES };
    [self.centralManager scanForPeripheralsWithServices:nil options:option];
    
}

- (void)_authenticateReader {
    if (!self.bluetoothReader) {
        return;
    }
    
    NSData *defaultMasterKey = nil;
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        defaultMasterKey = [ABDHex byteArrayFromHexString:@"41 43 52 31 32 35 35 55 2D 4A 31 20 41 75 74 68"];
    }
    else if ([self.bluetoothReader isKindOfClass:[ABTAcr3901us1Reader class]]) {
        defaultMasterKey = [ABDHex byteArrayFromHexString:@"FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF"];
    }
    if (!defaultMasterKey) {
        return;
    }
    
    [self.bluetoothReader authenticateWithMasterKey:defaultMasterKey];
    
}

- (void)_startPollongProcess {
    self.escapeCmdType = RTAcsBtEscCmdTypeTransmitPower;
    [self _configureTransmitPower];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.escapeCmdType = RTAcsBtEscCmdTypeSleepMode;
        [self _configureSleepMode];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.escapeCmdType = RTAcsBtEscCmdTypeEnablePolling;
        [self _enablePolling];
    });
}

- (void)_configureTransmitPower {
    if (!self.bluetoothReader) {
        return;
    }
    
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        
        uint8_t command[] = { 0xE0, 0x00, 0x00, 0x49, 0x00 };
        switch (_transmitPower) {
            case RTAcsBtTransmitPowerLow: {
                command[4] = 0x01;
                break;
            }
            case RTAcsBtTransmitPowerMedium: {
                command[4] = 0x02;
                break;
            }
            case RTAcsBtTransmitPowerHigh: {
                command[4] = 0x03;
                break;
            }
            default:
                break;
        }
        
        RTRfidLog(@"send escape command: %@", [ABDHex hexStringFromByteArray:command length:sizeof(command)]);
        [self.bluetoothReader transmitEscapeCommand:command length:sizeof(command)];
    }
    
}

- (void)_configureSleepMode {
    if (!self.bluetoothReader) {
        return;
    }
    
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        
        uint8_t command[] = { 0xE0, 0x00, 0x00, 0x48, 0x04 };
        switch (_sleepMode) {
            case RTAcsBtSleepModeAfter60s: {
                command[4] = 0x00;
                break;
            }
            case RTAcsBtSleepModeAfter90s: {
                command[4] = 0x01;
                break;
            }
            case RTAcsBtSleepModeAfter120s: {
                command[4] = 0x02;
                break;
            }
            case RTAcsBtSleepModeAfter180s: {
                command[4] = 0x03;
                break;
            }
            default:
                break;
        }
        
        RTRfidLog(@"send escape command: %@", [ABDHex hexStringFromByteArray:command length:sizeof(command)]);
        [self.bluetoothReader transmitEscapeCommand:command length:sizeof(command)];
    }
    
}

- (void)_enablePolling {
    if (!self.bluetoothReader) {
        return;
    }
    
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        
        uint8_t command[] = { 0xE0, 0x00, 0x00, 0x40, 0x01 };
        
        RTRfidLog(@"send escape command: %@", [ABDHex hexStringFromByteArray:command length:sizeof(command)]);
        [self.bluetoothReader transmitEscapeCommand:command length:sizeof(command)];
    }
}

- (void)_disablePolling {
    if (!self.bluetoothReader) {
        return;
    }
    
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        
        uint8_t command[] = { 0xE0, 0x00, 0x00, 0x40, 0x00 };
        
        RTRfidLog(@"send escape command: %@", [ABDHex hexStringFromByteArray:command length:sizeof(command)]);
        [self.bluetoothReader transmitEscapeCommand:command length:sizeof(command)];
    }
}

- (void)_getCardSerialNumber {
    if (!self.bluetoothReader) {
        return;
    }
    
    if ([self.bluetoothReader isKindOfClass:[ABTAcr1255uj1Reader class]]) {
        
        uint8_t command[] = { 0xFF, 0xCA, 0x00, 0x00, 0x00 };
        
        RTRfidLog(@"send escape command: %@", [ABDHex hexStringFromByteArray:command length:sizeof(command)]);
        [self.bluetoothReader transmitEscapeCommand:command length:sizeof(command)];
    }
}

/**
 * Returns the description from the card status.
 * @param cardStatus the card status.
 * @return the description.
 */
- (NSString *)ABD_stringFromCardStatus:(ABTBluetoothReaderCardStatus)cardStatus {
    
    NSString *string = nil;
    
    switch (cardStatus) {
            
        case ABTBluetoothReaderCardStatusUnknown:
            string = @"Unknown";
            break;
            
        case ABTBluetoothReaderCardStatusAbsent:
            string = @"Absent";
            break;
            
        case ABTBluetoothReaderCardStatusPresent:
            string = @"Present";
            break;
            
        case ABTBluetoothReaderCardStatusPowered:
            string = @"Powered";
            break;
            
        case ABTBluetoothReaderCardStatusPowerSavingMode:
            string = @"Power Saving Mode";
            break;
            
        default:
            string = @"Unknown";
            break;
    }
    
    return string;
}

/**
 * Returns the description from the battery status.
 * @param batteryStatus the battery status.
 * @return the description.
 */
- (NSString *)ABD_stringFromBatteryStatus:(ABTBluetoothReaderBatteryStatus)batteryStatus {
    
    NSString *string = nil;
    
    switch (batteryStatus) {
            
        case ABTBluetoothReaderBatteryStatusNone:
            string = @"No Battery";
            break;
            
        case ABTBluetoothReaderBatteryStatusFull:
            string = @"Full";
            break;
            
        case ABTBluetoothReaderBatteryStatusUsbPlugged:
            string = @"USB Plugged";
            break;
            
        default:
            string = @"Low";
            break;
    }
    
    return string;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    static BOOL firstRun = YES;
    NSString *message = nil;
    
    switch (central.state) {
        case CBCentralManagerStateUnknown:
        case CBCentralManagerStateResetting: {
            RTRfidLog(@"state:CBCentralManagerStateResetting");
            
            message = @"The update is being started. Please wait until Bluetooth is ready.";
            break;
        }
        case CBCentralManagerStateUnsupported: {
            RTRfidLog(@"state:CBCentralManagerStateUnsupported");
            
            message = @"This device does not support Bluetooth low energy.";
            break;
        }
        case CBCentralManagerStateUnauthorized: {
            RTRfidLog(@"state:CBCentralManagerStateUnauthorized");
            
            message = @"This app is not authorized to use Bluetooth low energy.";
            break;
        }
        case CBCentralManagerStatePoweredOff: {
            RTRfidLog(@"state:CBCentralManagerStatePoweredOff");
            
            [self.peripherals removeAllObjects];
            if (!firstRun) {
                message = @"You must turn on Bluetooth in Settings in order to use the reader.";
            }
            break;
        }
        default: {
            if (_isStarted) {
                [self _startBtScan];
            }
            break;
        }
    }
    
    if (message) {
        NSError *error = [NSError errorWithDomain:RTAcsBtErrorDomain code:-8000 userInfo:@{NSLocalizedDescriptionKey:message, NSLocalizedFailureReasonErrorKey:message}];
        [self _postDelegateError:error];
    }
    
    firstRun = NO;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    // If the peripheral is not found, then add it to the array.
    if ([_peripherals indexOfObject:peripheral] == NSNotFound) {
        
        RTRfidLog(@"discovered: %@", peripheral.name);
        [_peripherals addObject:peripheral];
        
        if (self.specificDeviceId && self.specificDeviceId.length > 0) {
            NSString *specificDeviceId = [self.specificDeviceId stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            if ([specificDeviceId rangeOfString:@"-"].length != 0) {
                specificDeviceId = [[specificDeviceId componentsSeparatedByString:@"-"] lastObject];
            }
            NSString *name = peripheral.name;
            if ([name rangeOfString:@"-"].length != 0) {
                name = [[name componentsSeparatedByString:@"-"] lastObject];
            }
            
            if ([specificDeviceId isEqualToString:name]) {
                [_centralManager connectPeripheral:peripheral options:nil];
            }
        }
        else {
            [_centralManager connectPeripheral:peripheral options:nil];
        }
        
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    
    // Detect the Bluetooth reader.
    [_bluetoothReaderManager detectReaderWithPeripheral:peripheral];
    
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    
    RTRfidLog(@"didFailToConnectPeripheral: %@, error: %@", peripheral.name, error);
    
    if ([peripheral.identifier.UUIDString isEqualToString:self.peripheral.identifier.UUIDString]) {
        self.peripheral = nil;
        
        self.connectedReaderInfo = nil;
        self.escapeCmdType = RTAcsBtEscCmdTypeNone;
        self.escapeCommand = nil;
        self.atrString = nil;
        
        if (self.bluetoothReader) {
            NS_DURING
            [self.bluetoothReader detach];
            NS_HANDLER
            NS_ENDHANDLER
        }
        self.bluetoothReader = nil;
        
        // Show the error
        if (error) {
            [self _postDelegateError:error];
        }
    }
    
    if ([_peripherals containsObject:peripheral]) {
        [_peripherals removeObject:peripheral];
    }
    
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    
    RTRfidLog(@"didDisconnectPeripheral: %@, error: %@", peripheral.name, error);
    
    if ([peripheral.identifier.UUIDString isEqualToString:self.peripheral.identifier.UUIDString]) {
        self.peripheral = nil;
        
        self.connectedReaderInfo = nil;
        self.escapeCmdType = RTAcsBtEscCmdTypeNone;
        self.escapeCommand = nil;
        self.atrString = nil;
        
        if (self.bluetoothReader) {
            NS_DURING
            [self.bluetoothReader detach];
            NS_HANDLER
            NS_ENDHANDLER
        }
        self.bluetoothReader = nil;
        
        if (!error && self.customError) {
            error = self.customError;
        }
        
        if (_isListening) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicatorDidDisconnectReader:withError:)]) {
                [self.delegate rtRfidCommunicatorDidDisconnectReader:self withError:error];
            }
        }
        
    }
    
    self.customError = nil;
    
    if ([_peripherals containsObject:peripheral]) {
        [_peripherals removeObject:peripheral];
    }
    
}

#pragma mark - ABTBluetoothReaderManagerDelegate

- (void)bluetoothReaderManager:(ABTBluetoothReaderManager *)bluetoothReaderManager
               didDetectReader:(ABTBluetoothReader *)reader
                    peripheral:(CBPeripheral *)peripheral
                         error:(NSError *)error {
    
    self.connectedReaderInfo = nil;
    self.escapeCommand = nil;
    self.atrString = nil;
    
    if (error) {
        // Show the error
        RTRfidLog(@"detect reader error: %@, %@", peripheral, error);
        
        self.customError = error;
        
        if ([_peripherals containsObject:peripheral]) {
            [_peripherals removeObject:peripheral];
        }
        
    }
    else {
        [self.centralManager stopScan];
        
        RTRfidLog(@"detect reader success: %@, %@", peripheral, reader);
        
        self.peripheral = peripheral;
        self.bluetoothReader = reader;
        
        reader.delegate = self;
        [reader attachPeripheral:peripheral];
        
    }
}


#pragma mark - Bluetooth Reader

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader didAttachPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    
    if (error != nil) {
        // Show the error
        RTRfidLog(@"didAttachPeripheral error: %@", error);
        
        self.customError = error;
        
        self.peripheral = nil;
        self.bluetoothReader = nil;
        
        [self _startBtScan];
    }
    else {
        RTRfidLog(@"didAttachPeripheral: %@", peripheral);
        
        self.connectedReaderInfo = [[RTAcsBtReaderInfo alloc] init];
        
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoSystemId];
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoModelNumberString];
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoSerialNumberString];
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoFirmwareRevisionString];
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoHardwareRevisionString];
        [bluetoothReader getDeviceInfoWithType:ABTBluetoothReaderDeviceInfoManufacturerNameString];
        
        if (!self.specificDeviceId) {
            [self _authenticateReader];
        }
        
    }
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
    didReturnDeviceInfo:(NSObject *)deviceInfo
                   type:(ABTBluetoothReaderDeviceInfo)type
                  error:(NSError *)error {
    
    
    if (error) {
        // Show the error
        RTRfidLog(@"didReturnDeviceInfo error: %@", error);
    }
    else {
        switch (type) {
            case ABTBluetoothReaderDeviceInfoSystemId:
                // Show the system ID.
                RTRfidLog(@"SystemID: %@", [ABDHex hexStringFromByteArray:(NSData *)deviceInfo]);
                self.connectedReaderInfo.systemID = [ABDHex hexStringFromByteArray:(NSData *)deviceInfo];
                break;
                
            case ABTBluetoothReaderDeviceInfoModelNumberString:
                // Show the model number.
                RTRfidLog(@"Model Number: %@", (NSString *) deviceInfo);
                self.connectedReaderInfo.modelNumber = (NSString *) deviceInfo;
                break;
                
            case ABTBluetoothReaderDeviceInfoSerialNumberString: {
                // Show the serial number.
                RTRfidLog(@"Serial Number: %@", (NSString *) deviceInfo);
                NSString *serialNumber = (NSString *)deviceInfo;
                self.connectedReaderInfo.serialNumber = serialNumber;
                
                if (self.specificDeviceId) {
                    NSString *specificDeviceId = [self.specificDeviceId stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    if (([serialNumber compare:specificDeviceId options:NSCaseInsensitiveSearch] != NSOrderedSame) &&
                        [serialNumber rangeOfString:specificDeviceId].length == 0) {
                        
                        [bluetoothReader detach];
                        self.bluetoothReader = nil;
                        
                        self.customError = nil;
                        
                        [self.centralManager cancelPeripheralConnection:self.peripheral];
                        
                    }
                    else {
                        [self _authenticateReader];
                    }
                }
                
                break;
            }
            case ABTBluetoothReaderDeviceInfoFirmwareRevisionString:
                // Show the firmware revision.
                RTRfidLog(@"Firmware Revision: %@", (NSString *) deviceInfo);
                self.connectedReaderInfo.firmwareVersion = (NSString *) deviceInfo;
                break;
                
            case ABTBluetoothReaderDeviceInfoHardwareRevisionString:
                // Show the hardware revision.
                RTRfidLog(@"Hardware Revision: %@", (NSString *) deviceInfo);
                self.connectedReaderInfo.hardwareVersion = (NSString *) deviceInfo;
                break;
                
            case ABTBluetoothReaderDeviceInfoManufacturerNameString:
                // Show the manufacturer name.
                RTRfidLog(@"Manufacturer: %@", (NSString *) deviceInfo);
                self.connectedReaderInfo.manufacturerName = (NSString *) deviceInfo;
                break;
                
            default:
                break;
        }
    }
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader didAuthenticateWithError:(NSError *)error {
    
    if (error) {
        // Show the error
        RTRfidLog(@"didAuthenticateWithError: %@", error);
        
        self.customError = error;
        
        [bluetoothReader detach];
        self.bluetoothReader = nil;
        
        [self.centralManager cancelPeripheralConnection:self.peripheral];
        
    }
    else {
        RTRfidLog(@"didAuthenticateWithError success");
        
        [self _startPollongProcess];
        
    }
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
           didReturnAtr:(NSData *)atr
                  error:(NSError *)error {
    
    RTRfidLog(@"didReturnAtr: %@ error: %@", atr, error);
    if (error) {
        // Show the error
        [self _postDelegateError:error];
        
    }
    else {
        // Show the ATR string.
        self.atrString = [ABDHex hexStringFromByteArray:atr];
        
        self.escapeCmdType = RTAcsBtEscCmdTypeGetTagID;
        [self _getCardSerialNumber];
    }
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader didPowerOffCardWithError:(NSError *)error {
    
    RTRfidLog(@"didPowerOffCardWithError: %@", error);
    // Show the error
    if (error) {
        
    }
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
    didReturnCardStatus:(ABTBluetoothReaderCardStatus)cardStatus
                  error:(NSError *)error {
    
    RTRfidLog(@"didReturnCardStatus: %@, error: %@", [self ABD_stringFromCardStatus:cardStatus], error);
    if (error) {
        // Show the error
        [self _postDelegateError:error];
        
    }
    else {
        // Show the card status.
    }
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
  didReturnResponseApdu:(NSData *)apdu
                  error:(NSError *)error {
    
    RTRfidLog(@"didReturnResponseApdu: %@, error: %@", apdu, error);
    if (error) {
        // Show the error
    }
    else {
        // Show the response APDU.
    }
    
    if (_isListening) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didResponseToApdu:error:)]) {
            [self.delegate rtAcsBtCommunicator:self
                             didResponseToApdu:apdu
                                         error:error];
        }
    }
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
didReturnEscapeResponse:(NSData *)response
                  error:(NSError *)error {
    
    RTAcsBtEscCmdType cmdType = self.escapeCmdType;
    self.escapeCmdType = RTAcsBtEscCmdTypeNone;
    
    RTRfidLog(@"didReturnEscapeResponse: %@, error: %@", response, error);
    if (error) {
        // Show the error
    }
    else {
        // Show the escape response.
    }
    
    switch (cmdType) {
        case RTAcsBtEscCmdTypeEnablePolling: {
            if (_isListening) {
                if (!error) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didConnectToReader:)]) {
                        [self.delegate rtAcsBtCommunicator:self didConnectToReader:bluetoothReader];
                    }
                }
                else {
                    [self _postDelegateError:error];
                }
            }
            
            break;
        }
        case RTAcsBtEscCmdTypeGetTagID: {
            
            uint8_t *responseByte = (uint8_t *)response.bytes;
            
            NSString *tagID = [[ABDHex hexStringFromByteArray:response] uppercaseString];
            NSError *error = nil;
            if ([tagID hasSuffix:@"90 00"]) {
                tagID = [tagID substringToIndex:tagID.length-5];
                tagID = [tagID stringByReplacingOccurrencesOfString:@" " withString:@""];
            }
            else if ([tagID hasSuffix:@"62 82"]) {
                error = [NSError errorWithDomain:ABTErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:@"End of UID/ATS reached before Le bytes (Le is greater than UID Length)", NSLocalizedFailureReasonErrorKey:@"End of UID/ATS reached before Le bytes (Le is greater than UID Length)"}];
                
            }
            else if ([tagID hasSuffix:@"63 00"]) {
                error = [NSError errorWithDomain:ABTErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:@"The operation is failed", NSLocalizedFailureReasonErrorKey:@"The operation is failed"}];
                
            }
            else if ([tagID hasSuffix:@"6A 81"]) {
                error = [NSError errorWithDomain:ABTErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:@"Function not supported", NSLocalizedFailureReasonErrorKey:@"Function not supported"}];
                
            }
            else if (responseByte[response.length-2] == 0x6C) {
                NSString *errorMsg = [NSString stringWithFormat:@"Wrong length (wrong number Le: ‘%x’ encodes the exact number) if Le is less than the available UID length", responseByte[response.length-1]];
                error = [NSError errorWithDomain:ABTErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:errorMsg, NSLocalizedFailureReasonErrorKey:errorMsg}];
                
            }
            else {
                error = [NSError errorWithDomain:ABTErrorDomain code:-8001 userInfo:@{NSLocalizedDescriptionKey:@"Other failure", NSLocalizedFailureReasonErrorKey:@"Other failure"}];
                
            }
            
            if (_isListening) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(rtRfidCommunicator:didFindTag:withAtr:error:)]) {
                    [self.delegate rtRfidCommunicator:self
                                           didFindTag:tagID
                                              withAtr:self.atrString
                                                error:error];
                }
            }
            
            break;
        }
        default: {
            
            if (_isListening) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didResponseToEscapeCommand:error:)]) {
                    [self.delegate rtAcsBtCommunicator:self
                            didResponseToEscapeCommand:response
                                                 error:error];
                }
            }
            
            break;
        }
    }
    
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
    didChangeCardStatus:(ABTBluetoothReaderCardStatus)cardStatus
                  error:(NSError *)error {
    
    RTRfidLog(@"didChangeCardStatus: %@, error: %@", [self ABD_stringFromCardStatus:cardStatus], error);
    if (error) {
        // Show the error
        [self _postDelegateError:error];
        
    }
    else {
        // Show the card status.
        switch (cardStatus) {
            case ABTBluetoothReaderCardStatusPresent: {
                self.atrString = nil;
                
                // activate card.
                [bluetoothReader powerOnCard];
                break;
            }
            case ABTBluetoothReaderCardStatusPowered: {
                
                break;
            }
            default:
                break;
        }
        
        if (_isListening) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didChangeCardStatus:)]) {
                [self.delegate rtAcsBtCommunicator:self didChangeCardStatus:cardStatus];
            }
        }
        
    }
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
 didChangeBatteryStatus:(ABTBluetoothReaderBatteryStatus)batteryStatus
                  error:(NSError *)error {
    
    RTRfidLog(@"didChangeBatteryStatus: %@, error: %@", [self ABD_stringFromBatteryStatus:batteryStatus], error);
    if (error) {
        // Show the error
        
    }
    else {
        // Show the battery status.
        
    }
    
    if (_isListening) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didChangeBatteryLevel:error:)]) {
            [self.delegate rtAcsBtCommunicator:self
                        didChangeBatteryStatus:batteryStatus
                                         error:error];
        }
    }
    
}

- (void)bluetoothReader:(ABTBluetoothReader *)bluetoothReader
  didChangeBatteryLevel:(NSUInteger)batteryLevel
                  error:(NSError *)error {
    
    RTRfidLog(@"didChangeBatteryLevel: %@, error: %@", @(batteryLevel), error);
    if (error) {
        // Show the error
        
    }
    else {
        // Show the battery level.
        
    }
    
    if (_isListening) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(rtAcsBtCommunicator:didChangeBatteryLevel:error:)]) {
            [self.delegate rtAcsBtCommunicator:self
                         didChangeBatteryLevel:batteryLevel
                                         error:error];
        }
    }
    
}

@end
