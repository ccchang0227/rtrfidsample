//
//  RTRfidCommunicator.m
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "RTRfidCommunicator.h"


static BOOL _logging = YES;

@implementation RTRfidCommunicator
@synthesize delegate = _delegate;

+ (void)setLogging:(BOOL)logging {
    _logging = logging;
}

+ (BOOL)isLogging {
    return _logging;
}

- (void)start {
    assert(NO);
}

- (void)stop {
    assert(NO);
}

- (void)sendApdu:(NSData *)apdu {
    assert(NO);
}

- (void)powerOffCard {
    assert(NO);
}

@end
