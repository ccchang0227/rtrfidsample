//
//  RTRfidCommunicator.h
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef RTRfidLog
#define RTRfidLog(...) if ([RTRfidCommunicator isLogging]) { \
                            NSLog(__VA_ARGS__); \
                        }
#endif

@protocol RTRfidCommunicatorDelegate;

@interface RTRfidCommunicator : NSObject

@property (weak, nonatomic) id<RTRfidCommunicatorDelegate> delegate;

- (void)start;
- (void)stop;

- (void)sendApdu:(NSData *)apdu;

// power off the presented card.
- (void)powerOffCard;

+ (void)setLogging:(BOOL)logging;
+ (BOOL)isLogging;

@end

@protocol RTRfidCommunicatorDelegate <NSObject>
@optional

- (void)rtRfidCommunicator:(RTRfidCommunicator *)communicator didReceiveError:(NSError *)error;

- (void)rtRfidCommunicatorDidDisconnectReader:(RTRfidCommunicator *)communicator withError:(NSError *)error;

// invoked after card is powered on.
- (void)rtRfidCommunicator:(RTRfidCommunicator *)communicator
                didFindTag:(NSString *)tagID
                   withAtr:(NSString *)atrString
                     error:(NSError *)error;

@end
