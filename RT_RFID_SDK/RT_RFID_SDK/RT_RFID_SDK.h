//
//  RT_RFID_SDK.h
//  RT_RFID_SDK
//
//  Created by realtouchapp on 2017/10/13.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RT_RFID_SDK.
FOUNDATION_EXPORT double RT_RFID_SDKVersionNumber;

//! Project version string for RT_RFID_SDK.
FOUNDATION_EXPORT const unsigned char RT_RFID_SDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RT_RFID_SDK/PublicHeader.h>


#import <RT_RFID_SDK/RTAcsBtCommunicator.h>
#import <RT_RFID_SDK/RTFeitianAjCommunicator.h>
#import <RT_RFID_SDK/ABDHex.h>
