//
//  RTAcsBtCommunicator.h
//  RTRFIDSample
//
//  Created by realtouchapp on 2017/10/2.
//  Copyright © 2017年 realtouchapp. All rights reserved.
//

#import "RTRfidCommunicator.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ACSBluetooth.h"


typedef NS_ENUM(NSInteger, RTAcsBtTransmitPower) {
    RTAcsBtTransmitPowerVeryLow,
    RTAcsBtTransmitPowerLow,
    RTAcsBtTransmitPowerMedium,
    RTAcsBtTransmitPowerHigh
};

typedef NS_ENUM(NSInteger, RTAcsBtSleepMode) {
    RTAcsBtSleepModeNone,
    RTAcsBtSleepModeAfter60s,
    RTAcsBtSleepModeAfter90s,
    RTAcsBtSleepModeAfter120s,
    RTAcsBtSleepModeAfter180s
};

FOUNDATION_EXPORT NSString *const RTAcsBtCommunicatorTransmitPowerOptionKey;
FOUNDATION_EXPORT NSString *const RTAcsBtCommunicatorSleepModeOptionKey;

@protocol RTAcsBtCommunicatorDelegate;

/**
 * Class for simply use ACS bluetooth reader (ACR1255U and ACR390U series)
 *
 * @version 0.7.0
 * @author Chih-chieh Chang
 * @date 2017-10-16
 */
@interface RTAcsBtCommunicator : RTRfidCommunicator <CBCentralManagerDelegate, ABTBluetoothReaderManagerDelegate, ABTBluetoothReaderDelegate> {
@package
    CBCentralManager *_centralManager;
    CBPeripheral *_peripheral;
    ABTBluetoothReaderManager *_bluetoothReaderManager;
    ABTBluetoothReader *_bluetoothReader;
}

@property (strong, nonatomic) NSString *specificDeviceId;

@property (nonatomic) RTAcsBtTransmitPower transmitPower;
@property (nonatomic) RTAcsBtSleepMode sleepMode;

+ (instancetype)communicator;
+ (instancetype)communicatorWithSpecificDeviceID:(NSString *)specificDeviceID;
+ (instancetype)communicatorWithSpecificDeviceID:(NSString *)specificDeviceID
                                         options:(NSDictionary *)options;

- (instancetype)init;
- (instancetype)initWithSpecificDeviceID:(NSString *)specificDeviceID;
- (instancetype)initWithSpecificDeviceID:(NSString *)specificDeviceID
                                 options:(NSDictionary *)options  NS_DESIGNATED_INITIALIZER;

@property (weak, nonatomic) id<RTAcsBtCommunicatorDelegate, RTRfidCommunicatorDelegate> delegate;

// -- reader's informations. Returns nil if there is no reader connected.
@property (readonly, nonatomic) NSString *readerName;
@property (readonly, nonatomic) NSString *serialNumber;
@property (readonly, nonatomic) NSString *firmwareVersion;
@property (readonly, nonatomic) NSString *hardwareVersion;
@property (readonly, nonatomic) NSString *systemID;
@property (readonly, nonatomic) NSString *modelNumber;
@property (readonly, nonatomic) NSString *manufacturerName;

- (void)forceDisconnect;

- (void)sendEscapeCommand:(NSData *)escapeCommand;

@end

@protocol RTAcsBtCommunicatorDelegate <RTRfidCommunicatorDelegate>
@optional

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
         didConnectToReader:(ABTBluetoothReader *)reader;

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
        didChangeCardStatus:(ABTBluetoothReaderCardStatus)cardStatus;

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
 didResponseToEscapeCommand:(NSData *)response
                      error:(NSError *)error;

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
          didResponseToApdu:(NSData *)response
                      error:(NSError *)error;

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
     didChangeBatteryStatus:(ABTBluetoothReaderBatteryStatus)batteryStatus
                      error:(NSError *)error;

- (void)rtAcsBtCommunicator:(RTAcsBtCommunicator *)communicator
      didChangeBatteryLevel:(NSUInteger)batteryLevel
                      error:(NSError *)error;

@end
